//Set Up Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors");

//Access to Routes used in the Application
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");


//Server SetUp
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//DB Connection
mongoose.set("strictQuery", true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.vx1mcmz.mongodb.net/capstone-2-comia-248?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

//Connection to DB
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"));

//Add the Routes 
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


//Server listening
app.listen(process.env.PORT || 4000, ()=>{

	console.log(`API is now online on port ${process.env.PORT || 4000}`)

})  