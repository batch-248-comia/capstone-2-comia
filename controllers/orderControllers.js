//Import

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Exports

//Add Order 
module.exports.addOrder = async (data) =>{

	console.log(data)
	let getUser = await User.findById(data.userId).then(user=>{


		let userOrder = new Order({
			userId: data.userId,
			totalAmount : data.totalAmount,
			products: [
			{
				productId: data.productId,
				quantity: data.quantity
			}
		]
		});


		return userOrder.save().then((order1,error)=>{

			if(error){
				return false;
			}else{
				console.log (data)
				let productUpdate = Product.findById(data.productId).then(product1=>{

					product1.orders.push({
						orderId: order1.id,
						quantity: data.quantity
				})
			product1.save()

			return Order.findByIdAndUpdate(order1.id).then(savedOrder=>{

				if(error){
					return false
				}else{
					 savedOrder.products.push({
						productId:data.order.productId,
						quantity:data.order.quantity
					})
				}

			
				savedOrder.totalAmount = product1.price * data.order.quantity;
				console.log(savedOrder)
				return savedOrder.save()
			})
		
			return product1.save()
		})
			return true
		}
	})
})
 	if(getUser){

 		return Product.findById(data.productId).then(orderedProduct=>{
 			return User.findById(data.userId).then(foundUserId=>{
 				totalAmount = orderedProduct.price * data.quantity;

 				//return (`Order details: User ID:${foundUserId}, ProductID: ${orderedProduct}, Total Amount: ${totalAmount}. `)
 				return true
 			})
 		})
 	}else{
 		return false
 	}
 };

//Retrieve All Orders
module.exports.getAllOrders = () => {

	return Order.find({}).then(result=>{
		return result;
	});
};

//Retrieve All Orders
module.exports.getUserOrders = (userId) => {

	return Order.findById(userId).then(result=>{
		console.log(userId)
		return result;
	});
};

