//Import

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Exports

//Check if user email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result=>{
		
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};

//User registration
module.exports.registerUser = (reqBody) =>{

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
		mobileNo: reqBody.mobileNo
	});

	return newUser.save().then((user,error)=>{

		if(error){
			return false
		}else{
			return true
		};
	});
};

//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)}
			}else{
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result=>{

		result.password = "";
		return result;
	})
}


//Set As Admin
module.exports.updateAdmin = (reqParams, reqBody) => {

	let updatedAdmin = {

		isAdmin: true
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((result,error)=>{

		if(error){
			return false
		}else{
			return result;
		};
	});
};

