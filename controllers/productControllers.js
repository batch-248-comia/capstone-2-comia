//Import

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Exports

//Create Product (Admin Only)
module.exports.addProduct = (data) => {

	if(data.isAdmin){

		let newProduct = new Product ({

		name: data.product.name,
		description: data.product.description,
		price: data.product.price,
		imageLink: data.product.imageLink

	});

	return newProduct.save().then((product,error)=>{

		if(error){
			return false
		}else{
			return true
		}
	})
	}else{
		return false
	}
};  

//Get All Active Products
module.exports.getAllActiveProducts = () =>{

	return Product.find({isActive: true}).then(result=>{
		return result;
	});

};

//Get All Products
module.exports.getAllProducts = () =>{

	return Product.find({}).then(result=>{
		return result;
	});

};
 
//Get Single Product
module.exports.getOneProduct = (productId) => {

	return Product.findById(productId).then(result=>{
		console.log(productId)
		return result;
	})
}

//Update single Product 
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price: reqBody.price
	}
	console.log(reqParams)
	//findByIdAndUpdate(document ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=>{
		console.log(product)
		if(error){
			return false
		}else{
			return true;
		};
	});
};

//Archive Product
module.exports.archiveProduct = (reqParams) => {

			let updateActiveField = {
				isActive : false
			};

			return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

				// Product not archived
				if (error) {

					return false;

				// Product archived successfully
				} else {

					return true;

				}

			});
		};

//Unarchive a product
/*module.exports.unarchiveProduct = (reqParams) => {
	
	return Product.findByIdAndUpdate(reqParams.productId).then((result,error)=>{

		if(error){
			return false
		}else{
			return true;
		};
	});
};
*/
module.exports.unarchiveProduct = (reqParams, reqBody) => {

			let updateActiveField = {
				isActive : true
			};

			return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

				// Product not archived
				if (error) {

					return false;

				// Product archived successfully
				} else {

					return true;

				}

			});
		};

