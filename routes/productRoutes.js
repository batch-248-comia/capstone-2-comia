//Import

const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Routes

//Route for creating a product if admin
router.post("/addProduct",auth.verify,(req,res)=>{

	
	const data = {

		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController=>res.send(resultFromController));
})

//Route for all active products
router.get("/allActiveProducts",(req,res)=>{

	productController.getAllActiveProducts().then(resultFromController=>res.send(resultFromController));

});

//Route for all products
router.get("/all",(req,res)=>{

	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));

});

//Route for single product
router.get("/:productId",(req,res)=>{ 

	productController.getOneProduct(req.params.productId).then(resultFromController=>res.send(resultFromController));

})

//Route for updating product
router.put("/:productId", auth.verify, (req,res)=>{

	productController.updateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController));

});

//Route for Archiving Products 
router.put("/archive/:productId", auth.verify, (req, res) => {

	productController.archiveProduct(req.params).then(resultFromController=>res.send(resultFromController));

});

//Unarchive Products
/*router.put("/unarchive/:productId", auth.verify, (req,res)=>{

	productController.unarchiveProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController));

});*/
router.put("/unarchive/:productId",auth.verify,(req,res)=>{

	productController.unarchiveProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController))
});


module.exports = router;

