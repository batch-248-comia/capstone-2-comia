//Import

const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Routes

//Route for checking if User Email exists
router.post("/checkEmailExists",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
});

//Route for user registration
router.post("/register",(req,res)=>{

	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//Route for user authentication
router.post("/login",(req,res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//Route for retrieving user details

router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));

})

//Set as Admin
router.put("/updateAdmin/:userId", auth.verify, (req,res)=>{

	userController.updateAdmin(req.params, req.body).then(resultFromController=>res.send(resultFromController));

});

module.exports = router;