//Import

const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

//Routes
//Route for retrieving all the orders
router.get("/cart/:userId",(req,res)=>{

	orderController.getUserOrders().then(resultFromController=>res.send(resultFromController));

});


//Route for creating an order non-admin
router.post("/cart/:userId",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		totalAmount: req.body.totalAmount,
		productId: req.body.productId,
		quantity:req.body.quantity
	}
	console.log(req.headers.authorization)
	orderController.addOrder(data).then(resultFromController=>res.send(resultFromController));
})

//Route for retrieving all the orders
router.get("/getAllOrders",(req,res)=>{

	orderController.getAllOrders().then(resultFromController=>res.send(resultFromController));

});


module.exports = router;

